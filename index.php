<?php
if (isPOST()) {
  $tmpfile = $_FILES["imagen"];
  $ext_image = extension($tmpfile['name']);
  $crop_x = post('crop_x');
  $crop_y = post('crop_y');
  $crop_w = post('crop_w');
  $crop_h = post('crop_h');
  
  if ($ext_image == 'jpg' || $ext_image == 'jpeg') $im = imagecreatefromjpeg($tmpfile['tmp_name']);
  else if ($ext_image == 'png') $im = imagecreatefrompng($tmpfile['tmp_name']);

  $im2 = imagecrop($im, ['x' => $crop_x, 'y' => $crop_y, 'width' => $crop_w, 'height' => $crop_h]);

  if ($im2 !== FALSE) {
    imagepng($im2, 'example-cropped.png');
    imagedestroy($im2);
  }
  imagedestroy($im);
}

// Helpers
function isPOST() {
  if ($_SERVER['REQUEST_METHOD'] == "POST") return true;
  else return false;
}
function post($v) {
  $v1 = (isset($_POST[$v])) ? $_POST[$v] : '';
  return $v1;
}
function extension($archivo) {
  $exp = explode('.', $archivo);
  $e = end($exp);
  $e = mb_strtolower($e);
  if ($e=='jpeg') $e = 'jpg';
  else if ($e=='docx') $e = 'doc';
  else if ($e=='xlsx') $e = 'xls';
  else if ($e=='pptx') $e = 'ppt';
  return $e;
}
function console_log( $data ){
  echo '<script>';
  echo 'console.log('. json_encode( $data ) .')';
  echo '</script>';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
  <link rel="stylesheet" href="./cropperjs/cropper.css" referrerpolicy="no-referrer" />
  <title>Document</title>
  
  <style>
    img {
      display: block;
      max-width: 100%;
    }
    .preview {
      overflow: hidden;
      width: 200px; 
      height: 200px;
    }
    </style>
</head>
<body>
  <div class="container">
    <form id="form" action="index.php" method="post" enctype="multipart/form-data">
      <label for="imagen"></label>
      <input type="file" name="imagen" id="imagen" accept=".jpg, .png">
      <input type="number" name="crop_x" id="crop_x" hidden>
      <input type="number" name="crop_y" id="crop_y" hidden>
      <input type="number" name="crop_h" id="crop_h" hidden>
      <input type="number" name="crop_w" id="crop_w" hidden>
      <button type="submit">Recortar</button>
    </form>
  </div>

  <!-- Modal -->
  <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <div class="mb-3">
            <img id="image" src="" width="400">
          </div>
          <div class="preview"></div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
          <button type="button" id="crop_image" class="btn btn-primary">Save cropped image</button>
        </div>
      </div>
    </div>
  </div>

  <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
  <script src="./cropperjs/cropper.js"></script>

  <script>
    let cropper;

    $('#imagen').on('change', e => {
      if ($('#imagen').val() != "") {
        $('#exampleModal').modal('toggle');
        image.src = URL.createObjectURL(e.target.files[0]);
      }
    })

    $('#exampleModal').on('shown.bs.modal', () => {
      if (cropper) cropper.destroy();
      let image = document.getElementById('image');
      cropper = new Cropper(image, {
        aspectRatio: 160 / 187,
        autoCropArea: 0.7,
        viewMode: 2,
        center: true,
        dragMode: 'move',
        movable: true,
        scalable: true,
        guides: true,
        zoomOnWheel: true,
        cropBoxMovable: true,
        wheelZoomRatio: 0.1,
        preview: '.preview'
      });
    });

    $('#crop_image').click( () => {
      $('#crop_x').val(cropper.getData(true).x);
      $('#crop_y').val(cropper.getData(true).y);
      $('#crop_w').val(cropper.getData(true).width);
      $('#crop_h').val(cropper.getData(true).height);
      $('#exampleModal').modal('toggle');
    });
  </script>
</body>
</html>